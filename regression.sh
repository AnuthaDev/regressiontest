getmaxidx(){
echo $(ftdump $1 | grep "glyph count" | cut -d ':' -f 2 | tr -d ' ')
}

generatehash(){
for (( idx=0 ; idx<$2 ; idx++ )) ; do
ftgrid -k Pq -f ${idx} 72 $1 > /dev/null 2>&1
echo "Hashing ftgrid${idx}.png"
md5sum "ftgrid${idx}.png" >> md5.txt
rm "ftgrid${idx}.png"
done
}

checkfont(){

echo "Checking ${1} for regressions"
mkdir $1 && cd $1

maxidx=$(getmaxidx ../../fonts/$1/$1.ttf)

generatehash ../../fonts/$1/$1.ttf maxidx

hash=$(md5sum md5.txt | cut -d ' ' -f 1)

basehash=$(md5sum ../../fonts/$1/md5.txt | cut -d ' ' -f 1)

if [ "${hash}" = "${basehash}" ] ; then
echo "No errors found"

else

echo "ERROR! ERROR! ERROR! ERROR!"
fi

cd ..
}

mkdir test && cd test

checkfont CMap2
checkfont Composite
checkfont Distortable
checkfont UVSTest
